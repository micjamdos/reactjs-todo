# reactjs-todo

React todo app with simple sorting and browser storage. Tested working on Firefox and Brave on Ubuntu 22.04 and Windows 10

## Setup
Clone the repo and run
```
npm install
```

## Run
```
npm start
```