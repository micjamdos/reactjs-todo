import React, { useState, useEffect } from 'react';
import './App.css';
import { v4 as uuidv4 } from 'uuid';

// Bootstrap
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";

function App() {

  // Initialize states for setting todo values
  const [title, setTitle] = useState("");
  const [priority, setPriority] = useState("");
  const [details, setDetails] = useState("");

  // State to hide/reveal form
  const [form, setFormVisibility] = useState(false);

  // States to sort todos and hide sort button until todos are present
  const [sort, setSort] = useState("");
  const [sortVisibility, setSortVisibility] = useState("d-none");

  // If nothing is stored in localStorage, create empty array
  // Else, parse json retrieved from localStorage and populate todos
  const [todos, setTodos] = useState(
    () =>
      {
        if(JSON.parse(localStorage.getItem('todos')) !== null)
        {
          return JSON.parse(localStorage.getItem('todos'));
        }
        else
        {
          return [];
        }
      }
    );

  // Copy todos to separate array (avoids triggering useeffect hooks targeting todos)
  const [sortedTodos, setSortedTodos] = useState([]);

  // Add todo
  const addTodo = (e) => 
    {
      // Prevent default form submission behavior (prevents page refresh)
      e.preventDefault();

      // Prevent blank entries from being added
      if(title.trim() === "") return;

      // Create entries based on value received from form and randomly generated uuid
      setTodos([...todos, 
        {
          title: title,
          details: details,
          priority: priority,
          date: date,
          id: uuidv4(),
        }]);

      // After submitting, clear inputs
      setTitle("");
      setDetails("");
      setPriority("");
    };

    // Returns filtered list of all todos without the todo of matching todoID
    const removeTodo = (todoId) => 
    {
      setTodos(todos.filter(todo => todo.id !== todoId));
    }

    // Set todos array to blank
    const removeAllTodos = () =>
    {
      setTodos([]);
    }

    // Return prettified string based on todo.priority (for updating dropdown button selection text)
    const solvePriority = (priority) =>
    {
      if(priority !== "")
      {
        return  priority === "low" ? "Low Priority"
                : priority === "medium" ? "Medium Priority"
                : "High Priority";
      }
      else
      {
        return "Choose Priority..."
      }
    }

    // Return color value for various bootstrap components from todo.priority
    const solvePriorityBorder = (p) =>
    {
      return  p === "high" ? "danger"
              : p === "medium" ? "warning"
              : "success";
    }

    // Return arbitrary value for sorting priority
    const getSortPriorityValue = (p) =>
    {
      return  p ===  "high" ? 2 :
              p === "medium" ? 1 :
              0;
    }

    // Generate date string
    const date = new Date().toISOString();

    // Receive ISO date string, create date object, and print formatted date (allows different date formats to be printed)
    const parseDate = (d) =>
    {
      const dateobject = new Date(d);
      return dateobject.toLocaleString();
    }

    // Toggle form via button
    const toggleForm = () =>
    {
      setFormVisibility(!form);
    }

    // Show/hide form by returning css display value
    const isFormVisible = () =>
    {
      return form ? "d-block" : "d-none";
    }

    // Update alternating button text
    const solveFormButtonText = () =>
    {
      return form ? "Close Form" : "Add Todo";
    }

    // Return prettified string for the sort button
    const solveSort = (sort) =>
    {
      // return sort text
      return sort === "oldest" ? "Date (Oldest First)" :
                      sort === "newest" ? "Date (Newest First)" :
                      sort === "highest" ? "Priority (Highest First)" :
                      sort === "lowest" ? "Priority (Lowest First)" :
                      "Sort";
    }

    // Sorts a copy of [todos] inside of [sortedTodos]
    const sortTodos = (list, sorttype) =>
    {
      if(sorttype === "oldest")
      {
        setSortedTodos([...list.sort((a,b) => new Date(a.date).getTime() - new Date(b.date).getTime())]);
      }
      else if(sorttype === "highest")
      {
        setSortedTodos([...list.sort((a,b) => getSortPriorityValue(b.priority) - getSortPriorityValue(a.priority))])
      }
      else if(sorttype === "lowest")
      {
        setSortedTodos([...list.sort((a,b) => getSortPriorityValue(a.priority) - getSortPriorityValue(b.priority))])
      }
      else
      {
        setSortedTodos([...list.sort((a,b) => new Date(b.date).getTime() - new Date(a.date).getTime())]);
      }
    }

    // When todos state changes, update localStorage and update [sortedTodos] to reflect [todos]
    useEffect(() => 
    {
      localStorage.setItem('todos', JSON.stringify(todos));

      // Hides sort button if no todos
      if(todos.length != 0)
      {
        setSortVisibility("d-block");
      }
      else
      {
        setSortVisibility("d-none");
      }

      sortTodos(todos, sort)

    }, [todos])

    // Update [sortedTodos] whenever [sort] changes
    useEffect(() =>
    {
      sortTodos(todos, sort)
    }, [sort]);

  return (
    <div className="App" data-bs-theme="dark">
      <div className="container">

      <div className="d-flex justify-content-center my-3">
        <button className="btn btn-primary col-6" onClick={ () => toggleForm() }>{solveFormButtonText()}</button>
      </div>

        <form onSubmit={addTodo} className={ isFormVisible() + " pb-5" }>
          <div className="row">
            <div className="col-8 mx-auto">
              <div className="form-floating mb-1">
                <input 
                  autoFocus
                  value={title} 
                  onChange={(e) => setTitle(e.target.value)}
                  type="text" 
                  placeholder="Add todo..." 
                  className="form-control"
                />
                <label>Add todo title...</label>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-8 mx-auto">
              <div className="form-floating mb-1">
                <textarea 
                  autoFocus
                  value={details} 
                  onChange={(e) => setDetails(e.target.value)}
                  rows="3"
                  placeholder="Add details..." 
                  className="form-control"
                />
                <label>Add details...</label>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="d-flex col-8 justify-content-between mx-auto">
                <div className="dropdown col p-1">
                <button 
                  onChange={(e) => setPriority(e.target.value)} 
                  value={priority} 
                  className="btn btn-primary dropdown-toggle w-100" 
                  type="button" 
                  id="dropdownMenuButton" 
                  data-bs-toggle="dropdown" 
                  aria-haspopup="true" 
                  aria-expanded="false">
                    {solvePriority(priority)}
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a onClick={(e) => setPriority("low")} className="dropdown-item" href="#">Low Priority</a>
                    <a onClick={(e) => setPriority("medium")} className="dropdown-item" href="#">Medium Priority</a>
                    <a onClick={(e) => setPriority("high")} className="dropdown-item" href="#">High Priority</a>
                  </div>
                </div>

                <div className="col p-1">
                  <button className="btn btn-primary w-100" type="submit">Create Entry</button>
                </div>
              </div>
          </div>
        </form>

        <div className={"d-flex justify-content-end m-4 mt-5 pt-5 " + sortVisibility}>
          <div className="dropdown">
            <button 
              className="btn btn-primary dropdown-toggle w-100" 
              onChange={ (e) => setSort(e.value.target) }
              value={ solveSort(sort) }
              type="button"
              id="dropdownSortButton"
              data-bs-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false">
                {solveSort(sort)}
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownSortButton">
              <a onClick={(e) => setSort("newest")} className="dropdown-item" href="#">Date (Newest First)</a>
              <a onClick={(e) => setSort("oldest")} className="dropdown-item" href="#">Date (Oldest First)</a>
              <a onClick={(e) => setSort("highest")} className="dropdown-item" href="#">Priority (Highest First)</a>
              <a onClick={(e) => setSort("lowest")} className="dropdown-item" href="#">Priority (Lowest First)</a>
            </div>
          </div>
        </div>

        {sortedTodos.map((todo) => 
            (
              <div key={ todo.id } className={ "card m-4 border-3 border-" + solvePriorityBorder(todo.priority) } >
                <div className="card-header justify-content-between d-flex overflow-auto">
                  <strong>{ todo.title }</strong>
                  <span className={ "text-" + solvePriorityBorder(todo.priority)}>{solvePriority(todo.priority)}</span>
                </div>
                <div className="card-body">
                  <p className="card-text">{todo.details}</p>
                  <div className="d-flex align-items-center justify-content-between">
                    <div className="date">
                      <em>{ parseDate(todo.date) }</em>
                    </div>
                    <button onClick={ () => removeTodo(todo.id) } className="btn btn-danger remove">Remove</button>
                  </div>
                </div>
              </div>
            ))}          
        <div className="d-flex justify-content-center my-3">
          <button className="btn btn-primary col-6" onClick={ () => removeAllTodos() }>Clear list</button>
        </div>
      </div>
    </div>
  );
}

export default App;
